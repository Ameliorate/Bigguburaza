#!/bin/python3
import asyncio
import inspect
import re
import traceback

from quarry.net.auth import Profile
from quarry.net.client import ClientFactory, ClientProtocol
from twisted.internet import defer, reactor
from twisted.logger import globalLogPublisher
from twisted.logger._levels import LogLevel
from twisted.python.failure import Failure

from bigbrother import chat
from bigbrother.config import CONFIG
from bigbrother.discord_ import discord_init


class BigBrotherClientProtocol(ClientProtocol):
    def packet_keep_alive(self, buff):
        identifier = buff.unpack_varint(signed=True)
        self.send_packet('keep_alive', self.buff_type.pack_varint(
            identifier, signed=True))
        global COMMANDS
        chat.process_chat(self)
        # Packing Client Settings packet
        buffer = self.buff_type.pack_string('en_GB') + \
            self.buff_type.pack('b', 1) + \
            self.buff_type.pack_varint(0, signed=True) + \
            self.buff_type.pack('?', True) + \
            self.buff_type.pack('B', 0x01 | 0x02 | 0x04 | 0x08 | 0x10 | 0x20 | 0x40) + \
            self.buff_type.pack_varint(1)
        self.send_packet('client_settings', buffer)

    def packet_update_health(self, buff):
        health = buff.unpack('f')
        food = buff.unpack_varint()
        saturation = buff.unpack('f')

        if health <= 0:
            self.send_packet('client_status', self.buff_type.pack_varint(0))
            # chat.GroupChat('!').say('Please do not kill your Bigguburaza.')
            print('Died, respawning...')

    def packet_disconnect(self, buff):
        message = buff.unpack_chat()
        print('Disconnected for reason:', message)
        reactor.stop()

    def packet_chat_message(self, buff):
        chat_json = buff.unpack_json()
        message = chat.ServerToClientMessage(chat_json)
        position = buff.unpack("b")
        if position == 0:
            result = handle_chat(message, self)
            if not result:
                print(':: ' + str(message))
        elif position == 1:
            result = handle_chat(message, self)
            if not result:
                print(':=: ' + str(message))
        elif position == 2:
            print(':_: ' + str(message))
        else:
            self.logger.warning(
                'Unhandled chat position: ' + position + ' skipping.')
            return


class BigBrotherClientFactory(ClientFactory):
    protocol = BigBrotherClientProtocol


factory = BigBrotherClientFactory()


@defer.inlineCallbacks
def main():
    def error(err):
        print("Error in login: ", err)

    try:
        profile = yield Profile.from_credentials(
            CONFIG["auth"]["username"], CONFIG["auth"]["password"])
        factory.profile = profile
        yield factory.connect(CONFIG['server']['ip'],
                              int(CONFIG['server']['port']))
    except Exception as err:
        print("Error: ", err)


def handle_chat(message, protocol):
    """
    :return: True if the message was matched by anthing.
    """
    any_chat_hook_ran = False
    chat_hooks_copy = chat._CHAT_HOOKS.copy()
    for hook in chat_hooks_copy.keys():
        if re.match(hook, str(message)):
            hooks_copy = chat_hooks_copy[hook].copy()
            for to_run in hooks_copy:
                any_chat_hook_ran = True
                result = None
                try:
                    result = to_run(message)
                    if isinstance(result, defer.Deferred):
                        def errback(err):
                            if isinstance(err, Failure):
                                if err.check(chat.SessionCanceledException) is None:
                                    err.printTraceback()
                                    err.raiseException()
                        result.addErrback(errback)
                        result = False
                except chat.SessionCanceledException as e:
                    pass
                if result:
                    return True
    if any_chat_hook_ran:
        return True
    if message == 'You are already chatting in that group.':
        return True
    elif re.match(r'From Oniii_Chan: ssh', str(message)):
        chat.command(str(message)[21:])
        return True


def analyze(event):
    if event.get("log_level") == LogLevel.critical:
        print("Stopping for: ", event)
        reactor.stop()


globalLogPublisher.addObserver(analyze)


def aftermain():
    reactor.run()


if __name__ == '__main__':
    discord_init()
    main()
    aftermain()

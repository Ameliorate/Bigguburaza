import asyncio
import sys
import traceback
from asyncio import TimeoutError, ensure_future, wait_for
from datetime import datetime, timezone
from multiprocessing import Process, Queue

import discord as discordpy
import pytz
from humanhash import uuid

import bigbrother.snitchhandler
from bigbrother.config import CONFIG
from bigbrother.database import (DiscordServer, DiscordToNamelayerGroup,
                                 HumanIDtoDiscordServerID, NameLayerGroup)
from bigbrother.snitchhandler import SnitchHit, snitch_hook

snitch_queue = Queue()


def discord_init():
    proc = Process(target=discord_init_process,
                   name='discord', daemon=True, args=(CONFIG,))
    proc.start()


def discord_init_process(config):
    _client = discordpy.Client(loop=asyncio.get_event_loop())

    @_client.event
    async def on_ready():
        print('Logged into discord as',
              _client.user.name, _client.user.id, sep=' ')

    @_client.event
    async def on_error(event, *args, **kwargs):
        traceback.print_exc()
        _client.logout()
        sys.exit(1)

    @_client.event
    async def on_server_join(server: discordpy.Server):
        print('joined server ', server.name)
        id = None
        while True:
            id = uuid()[0]
            if HumanIDtoDiscordServerID.select(HumanIDtoDiscordServerID.human_id == id).count() != 0:
                continue
            HumanIDtoDiscordServerID.create(human_id=id, discord_id=server.id)
            break

        message = ("Bigguburaza-desu! Bigguburaza has been added to the server.\n"
                   "Whoever added Bigguburaza, please message the bot ingame `{0}`.\n"
                   "You can use `/r {0}` if Bigguburaza was the last person to message you.\n"
                   "This ID can also be used to refer to your server in commands,"
                   " please take note of it.").format(id)
        await _client.send_message(server, message)
        print("Added to {}, id: {}".format(server.name, server.id))

    @_client.event
    async def on_server_remove(server: discordpy.Server):
        human_id_row = HumanIDtoDiscordServerID.get(
            HumanIDtoDiscordServerID.discord_id == server.id)
        human_id_row.delete_instance()
        server_row = DiscordServer.get(discord_id=server.id)
        for server_to_group_row in DiscordToNamelayerGroup.select().where(
                DiscordToNamelayerGroup.discordserver == server_row):
            server_to_group_row.delete_instance()
        server_row.delete_instance()
        print('Removed from {}, {} owned by {}'.format(
            server.name, server.id, server.owner.name))

    ensure_future(snitch_loop(_client))
    _client.run(config['discord']['token'])


async def snitch_loop(_client):
    while True:
        if snitch_queue.empty():
            await asyncio.sleep(5)
            continue
        snitch = snitch_queue.get()
        group_row, _ = NameLayerGroup.get_or_create(name=snitch.group)
        for server_to_group in DiscordToNamelayerGroup.select().where(
                DiscordToNamelayerGroup.namelayergroup == group_row):
            message = '`{}` {}'.format(datetime.now(
                pytz.timezone('America/Detroit')), snitch.pretty_print)
            server = _client.get_server(
                server_to_group.discordserver.discord_id)
            if server is None:
                print('Server {} added by {} on group {} deleted, deleting records'.format(
                    server_to_group.discordserver.discord_id, server_to_group.authorized_user.name, group))
                server_row = server_to_group.discordserver
                server_to_group.delete_instance()
                server_row.delete_instance()
                continue
            snitch_channel = _client.get_channel(
                server_to_group.discordserver.snitch_channel_id)
            if snitch_channel is None:
                try:
                    snitch_channel = await wait_for(_client.create_channel(
                        server, server_to_group.discordserver.snitch_channel_name), 5)
                except TimeoutError as e:
                    print('Creating snitch channel named #{} timed out in {}'.format(
                        server_to_group.discordserver.snitch_channel_name,
                        server_to_group.discordserver.discord_id))
            try:
                try:
                    await wait_for(_client.send_message(snitch_channel, content=message), 5)
                except TimeoutError as e:
                    print('Sending snitch message in channel named #{} timed out in {}'.format(
                        server_to_group.discordserver.snitch_channel_name,
                        server_to_group.discordserver.discord_id))
            except discordpy.Forbidden as e:
                traceback.print_exc()
                print('No chat perms in {}, #{} ({}), {}'.format(
                    server.name, snitch_channel.name, snitch_channel.id, server.id))


def on_snitch(snitch: SnitchHit):
    snitch_queue.put(snitch)


snitch_hook(on_snitch)

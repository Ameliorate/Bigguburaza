import queue
import re
import textwrap
from typing import Callable

from twisted.internet.defer import Deferred
from parse import parse
from quarry.data.chat_styles import code_by_name, code_by_prop

from bigbrother.exception import ChatMessageTooLongError

_MESSAGE_QUEUE = queue.Queue()
_CHAT_HOOKS = {}


def _parse_json(obj, color=False):
    # Copied from quarry with slight modifications
    if isinstance(obj, str):
        return obj
    if isinstance(obj, list):
        return "".join((_parse_json(e, color=color) for e in obj))
    if isinstance(obj, dict):
        text = ""
        if color:
            for prop, code in code_by_prop.items():
                if obj.get(prop):
                    text += u"\u00a7" + code
            if "color" in obj:
                text += u"\u00a7" + code_by_name[obj["color"]]
        if "translate" in obj:
            text += obj["translate"]
            if "with" in obj:
                args = ", ".join((_parse_json(e, color=color)
                                  for e in obj["with"]))
                text += "{%s}" % args
        if "text" in obj:
            text += obj["text"]
        if "extra" in obj:
            text += _parse_json(obj["extra"], color=color)
        return text


class ServerToClientMessage:
    def __init__(self, message_json):
        self.message_json = message_json

    def __str__(self):
        return _parse_json(self.message_json, color=False)

    @property
    def text_nocolor(self):
        return str(self)

    @property
    def text_color(self):
        return _parse_json(self.message_json, color=True)

    @property
    def hover_text(self):
        def parse_hover(obj):
            if isinstance(obj, str):
                return obj
            if isinstance(obj, list):
                return "".join((parse_hover(e) for e in obj))
            if isinstance(obj, dict):
                text = ""
                if "hoverEvent" in obj:
                    text += _parse_json(obj["hoverEvent"]["value"])
                if "extra" in obj:
                    text += parse_hover(obj["extra"])
                return text
        return parse_hover(self.message_json)


class Chat:
    def say(self, message):
        raise NotImplementedError('Chat.say is a abstract method')


class GroupChat(Chat):
    def __init__(self, group: str):
        self.group = group

    def say(self, message):
        wrap = textwrap.wrap(message, 99 - len('/g {} '.format(self.group)))
        for msg in wrap:
            _MESSAGE_QUEUE.put('/g {} {}'.format(self.group, msg))


class PrivateMessage(Chat):
    def __init__(self, person):
        self.person = person

    def say(self, message):
        wrap = textwrap.wrap(message, 99 - len('/m {} '.format(self.person)))
        for msg in wrap:
            _MESSAGE_QUEUE.put('/m {} {}'.format(self.person, msg))

    def get_reply(self) -> Deferred:
        defered = Deferred()
        worker = None

        def get_reply_worker(msg: ServerToClientMessage) -> bool:
            result = parse('From {}: {{message}}'.format(
                self.person), str(msg))
            message = result['message'].strip()
            if message == 'cancel':
                self.say('Session canceled.')
                defered.errback(SessionCanceledException())
            else:
                defered.callback(message)
            nonlocal worker
            worker.remove()
            return True

        worker = chat_hook(
            '^From {}: .*$'.format(re.escape(self.person)), get_reply_worker)
        return defered


class LocalChat(Chat):
    def say(self, message):
        _MESSAGE_QUEUE.put('/e')
        wrap = textwrap.wrap(message, 99)
        for msg in wrap:
            _MESSAGE_QUEUE.put(msg)


class SessionCanceledException(Exception):
    pass


def process_chat(protocol):
    try:
        message = _MESSAGE_QUEUE.get_nowait()
        protocol.send_packet(
            "chat_message", protocol.buff_type.pack_string(message))
    except queue.Empty:
        pass


def command(message):
    if len(message) >= 100:
        raise ChatMessageTooLongError
    _MESSAGE_QUEUE.put(message)


def command_wrap(prefix, message):
    max_len = 99 - len(prefix)
    wrap = textwrap.wrap(message, max_len)
    for msg in wrap:
        command(prefix + msg)


def private_message(person: str, message: str):
    command_wrap('/m {} '.format(person), message)


def group_message(group: str, message: str):
    command_wrap('/g {} '.format(group), message)


class _ChatHook:
    def __init__(self, regex, hook):
        self.regex = regex
        self.hook = hook
        self.removed = False

    def remove(self):
        if not self.removed:
            _CHAT_HOOKS[self.regex].discard(self.hook)
            self.removed = True


def chat_hook(regex: str, hook: Callable[[ServerToClientMessage], bool]) -> _ChatHook:
    """
    Add a hook that'll be called every time the line of chat matches the given regex.
    :param regex: The regex the chat is matched against.
    :param hook: The function to be called. It is called with the chat message as it's only argument
    If true is returned, no other hooks are ran.
    """
    if regex in _CHAT_HOOKS:
        _CHAT_HOOKS[regex].add(hook)
        return _ChatHook(regex, hook)
    _CHAT_HOOKS[regex] = {hook}
    return _ChatHook(regex, hook)

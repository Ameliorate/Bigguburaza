from datetime import datetime

import peewee
from peewee import (BooleanField, DateTimeField, ForeignKeyField, IntegerField,
                    Model, PostgresqlDatabase, TextField, Proxy)

proxy = Proxy()


def migrate(db: PostgresqlDatabase):
    proxy.initialize(db)
    db.create_tables([Snitch, Player, NameLayerGroup,
                      SnitchNotification, Location])


class BaseModel(Model):
    class Meta:
        database = proxy


class NameLayerGroup(BaseModel):
    name = TextField(index=True, unique=True)


class Player(BaseModel):
    name = TextField(index=True, unique=True)


class Location(BaseModel):
    world = TextField(default='world')
    xpos = IntegerField()
    ypos = IntegerField()
    zpos = IntegerField()

    class Meta:
        indexes = (
            (('world', 'xpos', 'ypos', 'zpos'), True),
        )


class Snitch(BaseModel):
    name = TextField()
    group = ForeignKeyField(NameLayerGroup, related_name='snitches')
    snitchtype = IntegerField(default=0)  # noteblock=0, jukebox=1
    # If the snitch will give notifications if someone enters it.
    # False if it has been broken, culled, or otherwise.
    valid = BooleanField(default=True)
    dateinvalidated = DateTimeField(null=True, default=None)
    culltime = DateTimeField(null=True, default=None, index=True)
    location = ForeignKeyField(
        Location, related_name='snitch_there', index=True)
    timefirstseen = DateTimeField(default=datetime.now)

    class Meta:
        indexes = (
            (('valid', 'location'), True),
        )


class SnitchNotification(BaseModel):
    snitch = ForeignKeyField(Snitch, related_name='notifications', index=True)
    who = ForeignKeyField(Player, related_name='snitchtriggers', index=True)
    time = DateTimeField(default=datetime.now, index=True)
    action = IntegerField(default=0)  # entry=0, login=1, logout=2

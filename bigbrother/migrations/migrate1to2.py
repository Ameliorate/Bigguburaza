import peewee
from peewee import ForeignKeyField, Model, PostgresqlDatabase, Proxy, TextField

proxy = Proxy()


def migrate(db: PostgresqlDatabase):
    proxy.initialize(db)
    db.create_tables([DiscordServer, DiscordToNamelayerGroup])


class BaseModel(Model):
    class Meta:
        database = proxy


class Player(BaseModel):
    name = TextField(index=True, unique=True)


class NameLayerGroup(BaseModel):
    name = TextField(index=True, unique=True)


class DiscordServer(BaseModel):
    discord_id = TextField(unique=True)
    snitch_channel_id = TextField(null=True)
    snitch_channel_name = TextField(default='snitches')


class DiscordToNamelayerGroup(BaseModel):
    namelayergroup = ForeignKeyField(
        NameLayerGroup, related_name='discord_relay', index=True)
    discordserver = ForeignKeyField(
        DiscordServer, related_name='discord_relay_2', index=True)
    # Since we might not be able to check the rank of someone
    # using a command, this is the person who sent the invite
    # to the namelayer group ingame.
    authorized_user = ForeignKeyField(
        Player, related_name='authorized_discords', index=True)

import peewee
from humanhash import uuid
from peewee import ForeignKeyField, Model, PostgresqlDatabase, Proxy, TextField

proxy = Proxy()


def migrate(db: PostgresqlDatabase):
    proxy.initialize(db)
    db.create_tables([HumanIDtoDiscordServerID])

    for server in DiscordServer.select():
        while True:
            id, _ = uuid()
            if HumanIDtoDiscordServerID.select(HumanIDtoDiscordServerID.human_id == id).count() != 0:
                continue
            HumanIDtoDiscordServerID.create(
                human_id=id, discord_id=server.discord_id, maybe_discord_server=server)
            break


class BaseModel(Model):
    class Meta:
        database = proxy


class DiscordServer(BaseModel):
    discord_id = TextField(unique=True)
    snitch_channel_id = TextField(null=True)
    snitch_channel_name = TextField(default='snitches')


class HumanIDtoDiscordServerID(BaseModel):
    human_id = TextField(unique=True)
    discord_id = TextField()
    # If the channel was initalized, this is the fully initalized discord server.
    maybe_discord_server = ForeignKeyField(
        DiscordServer, related_name='human_id', index=True, null=True, unique=True)

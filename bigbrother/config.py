import configparser
import os.path
import sys

CONFIG_FILE = 'config.cfg'
CONFIG = configparser.ConfigParser()


def generate_config_file(name, config):
    config['auth'] = {'username': 'USERNAME',
                      'password': 'PASSWORD'}
    config['server'] = {'ip': 'mc.civclassic.com',
                        'port': '25565'}
    config['database'] = {'host': 'localhost',
                          'dbase': 'snitches',
                          'user': 'user',
                          'password': 'password'}
    config['discord'] = {'token': 'abcdefg',
                         'client_id': '12345',
                         'bot_add_url': 'https://tinyurl.com/'}
    with open(name, 'w') as configfile:
        configfile.write(
            '# Config automatically generated. Delete this file to reset.\n\n')
        config.write(configfile)
    print('Config file generated. Edit the username and password fields to set up an account.')
    sys.exit()


if os.path.isfile(CONFIG_FILE):
    CONFIG.read(CONFIG_FILE)
else:
    generate_config_file(CONFIG_FILE, CONFIG)

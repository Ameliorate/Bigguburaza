import re
import traceback

import peewee
from discord import HTTPException, Permissions
from discord.utils import oauth_url
from humanhash import uuid
from parse import parse
from twisted.internet.defer import (TimeoutError, ensureDeferred,
                                    inlineCallbacks)

from bigbrother.chat import (PrivateMessage, ServerToClientMessage, chat_hook,
                             command)
from bigbrother.config import CONFIG
from bigbrother.database import (DiscordServer, DiscordToNamelayerGroup,
                                 HumanIDtoDiscordServerID, NameLayerGroup,
                                 Player, db)


@inlineCallbacks
def group_invite(msg: ServerToClientMessage):
    result = parse(
        'You have been invited to the group {group} by {who}.\n'
        'Click this message to accept. If you wish to toggle invites '
        'so they always are accepted please run /autoaccept', str(msg))
    group = result['group']
    who = result['who']
    message_session = PrivateMessage(who)

    command('/nlag {}'.format(group))
    message_session.say("You've added Bigguburaza to a group.")
    message_session.say("Use `/r [SNITCH CHANNEL]` to reply to this message with the name of the "
                        "discord channel snitches will be relayed to.")
    message_session.say(
        "You can also use `/r cancel` at any time to end the session.")

    snitch_channel = yield message_session.get_reply()

    message_session.say(CONFIG['discord']['bot_add_url'])
    message_session.say("Please click the above link.")
    message_session.say(
        "After Bigguburaza is added to your server, it will say an ID in your #general channel.")
    message_session.say(
        "Please reply to this message with that ID, or \"cancel\" to end the session.")

    while True:
        id = yield message_session.get_reply()
        human_id_row = None
        try:
            human_id_row = HumanIDtoDiscordServerID.get(
                HumanIDtoDiscordServerID.human_id == id)
        except peewee.DoesNotExist as e:
            message_session.say(
                "Did you have a typo in the id? Please try again.")
            continue

        discord_id = human_id_row.discord_id
        snitch_channel_id = None
        for channel in server.channels:
            if channel.name == snitch_channel:
                snitch_channel_id = channel.id

        with db.transaction():
            authorized_player_row, _ = Player.get_or_create(name=who)
            group_row, _ = NameLayerGroup.get_or_create(name=group)
            discord_server_row = DiscordServer.create(
                discord_id=discord_id, snitch_channel_id=snitch_channel_id, snitch_channel_name=snitch_channel)
            HumanIDtoDiscordServerID.update(maybe_discord_server=discord_server_row).where(
                HumanIDtoDiscordServerID.discord_id == discord_id)
            mapping = DiscordToNamelayerGroup.create(
                namelayergroup=group_row, discordserver=discord_server_row, authorized_user=authorized_player_row)
        message_session.say("Successfully added to server.")
        message_session.say("Snitches will now be forewarded to the #{} channel in {}".format(
            snitch_channel, server.name))


chat_hook(
    '^You have been invited to the group \S* by [a-zA-Z0-9_]{1,16}.', group_invite)

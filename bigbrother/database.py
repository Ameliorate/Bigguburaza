from datetime import datetime
from multiprocessing import Event, current_process

import peewee
from peewee import (BooleanField, DateTimeField, DoesNotExist, ForeignKeyField,
                    IntegerField, IntegrityError, Model, PostgresqlDatabase,
                    TextField)

from bigbrother import snitchhandler
from bigbrother.config import CONFIG
from bigbrother.migrations import migrate0to1, migrate1to2, migrate2to3
from bigbrother.snitchhandler import (SnitchHit, SnitchNotificationType,
                                      SnitchType)

db = PostgresqlDatabase(CONFIG['database']['dbase'], user=CONFIG['database']['user'],
                        password=CONFIG['database']['password'], host=CONFIG['database']['host'])

DB_CURRENT_VERSION = 3
_MIGRATIONS = [migrate0to1.migrate,
               migrate1to2.migrate,
               migrate2to3.migrate]


def _database_snitch_handler(snitch: SnitchHit):
    with db.transaction():
        group_row, _ = NameLayerGroup.get_or_create(name=snitch.group)
        location_row, _ = Location.get_or_create(
            world=snitch.world, xpos=snitch.xpos, ypos=snitch.ypos, zpos=snitch.zpos)
        snitch_row = None
        try:
            snitch_row, _ = Snitch.get_or_create(
                name=snitch.snitch_name, group=group_row,
                snitchtype=snitch.snitch_type.value,
                location=location_row)
        except IntegrityError as e:
            snitch_row = Snitch.get(location=location_row, valid=True)
            snitch_row.valid = False
            snitch_row.dateinvalidated = datetime.now()
            snitch_row.save()
            snitch_row = Snitch.create(name=snitch.snitch_name, group=group_row,
                                       snitchtype=snitch.snitch_type.value,
                                       location=location_row)
        snitch_row.valid = True
        snitch_row.save()
        player_row, _ = Player.get_or_create(name=snitch.who)
        snitch_notification_row = SnitchNotification.create(
            snitch=snitch_row, who=player_row, action=snitch.notification_type.value)


snitchhandler.snitch_hook(_database_snitch_handler)

database_fully_inited_event = Event()


def init_db():
    db.connect()
    if current_process().name != 'MainProcess':
        database_fully_inited_event.wait()
        return
    with db.transaction():
        db.create_tables([DatabaseVersion], safe=True)
        version = None
        version_row = None
        try:
            version_row = DatabaseVersion.get()
            version = version_row.version
        except DoesNotExist as e:
            version = 0
        if version > DB_CURRENT_VERSION:
            print('The database version ({}) is above the curent version ({}). Please upgrade Bigguburaza and try again.')
            raise DatabaseVersionTooHighException()
        if version < DB_CURRENT_VERSION:
            for i in range(version, DB_CURRENT_VERSION):
                _MIGRATIONS[i](db)
            if version_row is not None:
                version_row.delete_instance()
            DatabaseVersion.create(version=DB_CURRENT_VERSION)
        database_fully_inited_event.set()


class DatabaseVersionTooHighException(Exception):
    pass


class BaseModel(Model):
    class Meta:
        database = db


class DatabaseVersion(BaseModel):
    version = IntegerField()


class NameLayerGroup(BaseModel):
    name = TextField(index=True, unique=True)


class Player(BaseModel):
    name = TextField(index=True, unique=True)


class Location(BaseModel):
    world = TextField(default='world')
    xpos = IntegerField()
    ypos = IntegerField()
    zpos = IntegerField()

    class Meta:
        indexes = (
            (('world', 'xpos', 'ypos', 'zpos'), True),
        )


class Snitch(BaseModel):
    name = TextField()
    group = ForeignKeyField(NameLayerGroup, related_name='snitches')
    snitchtype = IntegerField(default=0)  # noteblock=0, jukebox=1
    # If the snitch will give notifications if someone enters it.
    # False if it has been broken, culled, or otherwise.
    valid = BooleanField(default=True)
    dateinvalidated = DateTimeField(null=True, default=None)
    culltime = DateTimeField(null=True, default=None, index=True)
    location = ForeignKeyField(
        Location, related_name='snitch_there', index=True)
    timefirstseen = DateTimeField(default=datetime.now)

    class Meta:
        indexes = (
            (('valid', 'location'), True),
        )


class SnitchNotification(BaseModel):
    snitch = ForeignKeyField(Snitch, related_name='notifications', index=True)
    who = ForeignKeyField(Player, related_name='snitchtriggers', index=True)
    time = DateTimeField(default=datetime.now, index=True)
    action = IntegerField(default=0)  # entry=0, login=1, logout=2


class DiscordServer(BaseModel):
    discord_id = TextField(unique=True)
    snitch_channel_id = TextField(null=True)
    snitch_channel_name = TextField(default='snitches')


class DiscordToNamelayerGroup(BaseModel):
    namelayergroup = ForeignKeyField(
        NameLayerGroup, related_name='discord_relay', index=True)
    discordserver = ForeignKeyField(
        DiscordServer, related_name='discord_relay_2', index=True)
    # Since we might not be able to check the rank of someone
    # using a command, this is the person who sent the invite
    # to the namelayer group ingame.
    authorized_user = ForeignKeyField(
        Player, related_name='authorized_discords', index=True)


class HumanIDtoDiscordServerID(BaseModel):
    human_id = TextField(unique=True)
    discord_id = TextField()
    # If the channel was initalized, this is the fully initalized discord server.
    maybe_discord_server = ForeignKeyField(
        DiscordServer, related_name='human_id', index=True, null=True, unique=True)

from functools import partial
from enum import Enum
from typing import Callable

from parse import parse, search

from bigbrother.chat import ServerToClientMessage, chat_hook


_SNITCH_HOOKS = []


class SnitchNotificationType(Enum):
    enter = 0
    login = 1
    logout = 2

    @property
    def in_notification(self) -> str:
        if self == SnitchNotificationType.enter:
            return 'entered'
        if self == SnitchNotificationType.login:
            return 'logged in to'
        if self == SnitchNotificationType.logout:
            return 'logged out in'


class SnitchType(Enum):
    noteblock = 0
    jukebox = 1

    def from_str(snitchtype: str):
        if snitchtype == 'Entry':
            return SnitchType.noteblock
        if snitchtype == 'Logging':
            return SnitchType.jukebox

    @property
    def user_visable_str(self) -> str:
        if self == SnitchType.noteblock:
            return 'Entry'
        if self == SnitchType.jukebox:
            return 'Logging'


class SnitchHit:
    def __init__(self, who: str, notification_type: SnitchNotificationType,
                 group: str, snitch_type: SnitchType, snitch_name: str,
                 world: str, xpos: int, ypos: int, zpos: int):
        self.who = who
        self.notification_type = notification_type
        self.group = group
        self.snitch_type = snitch_type
        self.snitch_name = snitch_name
        self.world = world
        self.xpos = xpos
        self.ypos = ypos
        self.zpos = zpos

    def __str__(self):
        return '<bigbrother.snitchhandler.SnitchHit: ' + str({'who': self.who,
                                                              'notification_type': self.notification_type,
                                                              'group': self.group,
                                                              'snitch_type': self.snitch_type,
                                                              'snitch_name': self.snitch_name,
                                                              'world': self.world,
                                                              'xpos': self.xpos,
                                                              'ypos': self.ypos,
                                                              'zpos': self.zpos}) + '>'

    @property
    def pretty_print(self) -> str:
        a = '* {} {} snitch at {} [{} {} {} {}] Group: "{}" Type: "{}"'.format(
            self.who, self.notification_type.in_notification, self.snitch_name,
            self.world, str(self.xpos), str(self.ypos), str(self.zpos),
            self.group, self.snitch_type.user_visable_str)
        return a


def _on_snitch(msg: ServerToClientMessage, notification_type=None):
    who = parse_username(str(msg))

    hovertext = msg.hover_text
    hovertext_parsed = parse(
        'Location: [{world} {x:d} {y:d} {z:d}]\nGroup: {group}\nType: {type}\nName: {name}', hovertext)
    if hovertext_parsed is None:
        hovertext_parsed = parse(
            'Location: [{world} {x:d} {y:d} {z:d}]\nGroup: {group}\nType: {type}', hovertext)
        # unnamed snitch

    group = hovertext_parsed['group']
    snitchtype = SnitchType.from_str(hovertext_parsed['type'])
    snitch_name = ''
    try:
        snitch_name = hovertext_parsed['name']
    except KeyError as e:
        pass
    world = hovertext_parsed['world']
    xpos = hovertext_parsed['x']
    ypos = hovertext_parsed['y']
    zpos = hovertext_parsed['z']

    on_snitch(SnitchHit(who, notification_type, group, snitchtype,
                        snitch_name, world, xpos, ypos, zpos))


chat_hook("^§b \* [a-zA-Z0-9_]{1,16} entered snitch at \S* \[.* ?[\d\-]* [\d\-]* [\d\-]*]$",
          partial(_on_snitch, notification_type=SnitchNotificationType.enter))
chat_hook("^§b \* [a-zA-Z0-9_]{1,16} logged in to snitch at \S* \[.* ?[\d\-]* [\d\-]* [\d\-]*]$",
          partial(_on_snitch, notification_type=SnitchNotificationType.login))
chat_hook("^§b \* [a-zA-Z0-9_]{1,16} logged out in snitch at \S* \[.* ?[\d\-]* [\d\-]* [\d\-]*]$",
          partial(_on_snitch, notification_type=SnitchNotificationType.logout))


def on_snitch(snitch: SnitchHit):
    for hook in _SNITCH_HOOKS:
        hook(snitch)


def parse_username(msg: str):
    enter = search('§b * {} entered snitch at ', msg)
    login = search('§b * {} logged in to snitch at ', msg)
    logout = search('§b * {} logged out in snitch at ', msg)
    a = [enter, login, logout]
    return next(item for item in a if item is not None).fixed[0]


def snitch_hook(hook: Callable[[SnitchHit], None]):
    _SNITCH_HOOKS.append(hook)


def _print_snitches(snitch: SnitchHit):
    print(snitch.pretty_print)


snitch_hook(_print_snitches)
